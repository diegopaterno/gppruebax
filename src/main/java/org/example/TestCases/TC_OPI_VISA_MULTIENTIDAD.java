package org.example.TestCases;

import static org.hamcrest.Matchers.equalTo;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

import org.example.Tools.dbWorker;
import org.example.Tools.sshWorker;
import org.hamcrest.MatcherAssert;
import CentaJava.Core.Reports;
import io.restassured.path.json.JsonPath;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



@Test
public class TC_OPI_VISA_MULTIENTIDAD  {
	private String Status;
	private String esquema;




	public boolean Test(Reports report,String name, String configEntidad, String entidad) {
		
		System.out.println("\r\n##################################################################################################################################################################################################################"
				+ "##################################################################################################################################################################################################################\r\n");
		System.out.println("Configuring "+name+ "\r\n");


		//SET INSTANCES
		sshWorker opiCmd = new sshWorker();
		dbWorker oraResp = new dbWorker();

		//Set Variables
		boolean result = true;
		String[] res = new String[2];
		String f37Last6 = "0";
		//Set Reusltados Esperados
		String de39 = "00";
		String idAutorizacionAdquirente = "0000000";
		
		try {		

			//Set esquema Base de datos
			esquema = JsonPath.from(configEntidad).getString("ENTIDAD.esquema_db");

			//SET dbWorker
			oraResp.setUser(JsonPath.from(configEntidad).getString("DB.user"));
			oraResp.setPass(JsonPath.from(configEntidad).getString("DB.pass"));
			oraResp.setHost(JsonPath.from(configEntidad).getString("DB.host"));
			oraResp.setEntidad(JsonPath.from(configEntidad).getString("ENTIDAD.id_entidad"));
			
			System.out.println("##[command] : <------ Initializating Test: " + name + " ------>\r\n");
			report.AddLine("<------ Initializating Test: " + name + " ------>");
			
			//PRECONDICIONES
			System.out.println("##### INICIA EJECUCION DE PRECONDICIONES #####");
			report.AddLine("##### INICIA EJECUCION DE PRECONDICIONES #####");
					
			//CAMBIO DE ENTIDAD OPI
			opiCmd.stopOpiCmd(report, configEntidad, "opi4qa");
			Thread.sleep(1500);
			opiCmd.cambioEntidadSimulador(report, configEntidad, "opi4qa", entidad, "compras");
			Thread.sleep(1500);
			opiCmd.startOpiCmd(report, configEntidad, "opi4qa");
			Thread.sleep(1500);
			
			System.out.println("##### FINALIZA EJECUCION DE PRECONDICIONES #####");
			report.AddLine("##### FINALIZA EJECUCION DE PRECONDICIONES #####");
			
			//Preparación del archivo XML del TC
			System.out.println("##### PREPARACION DEL ARCHIVO XML DEL TC #####");
			report.AddLine("##### PREPARACION DEL ARCHIVO XML DEL TC #####");
			String xmlFile = Files.readString(Paths.get("./OPI_Files/" + entidad + "/VISA/TC_01_VI_MULTIENTIDAD.xml"));
			
			//Se valida el funcionamiento de OPI
			//if (!opiCmd.getOpiStatus(configEntidad, "opi4qa")) {
			//	MatcherAssert.assertThat("OPI no se encuentra operativo ", opiCmd.getOpiStatus(configEntidad, "opi4qa"), Matchers.equalTo(true));
			//}
			
			//EJECUCION OPI
			System.out.println("##### INICIO EJECUCION OPI #####");
			report.AddLine("##### INICIO EJECUCION OPI #####");
			
			//Enviar a OPI el archivo del tc
			opiCmd.sshSendCmdCreateXml(report, "xmlBaseAux.xml", configEntidad, "opi4qa", xmlFile);
			res = opiCmd.sshSendCmdGetDE37(report, "xmlBaseAux.xml", configEntidad, "opi4qa");
			idAutorizacionAdquirente = opiCmd.getIdAutorizacionAdquirente(report, configEntidad, "opi4qa");			
			
			System.out.println("idAutorizacionAdquirente obtenido: " + idAutorizacionAdquirente);
			
			if (res[0].equals(de39))
			{
				report.AddLine("Ejecucion Correcta<br>DE39: " + res[0]);
				System.out.println("##[section] : Ejecucion Correcta\r\nDE39: " + res[0] + "\r\n");
				//VALIDACIONES
				report.AddLine("idAutorizacionAdquirente: " + idAutorizacionAdquirente);
				System.out.println("##[section] : idAutorizacionAdquirente: " + idAutorizacionAdquirente + "\r\n");
				result = validacionGral(oraResp, report, idAutorizacionAdquirente);
			} else {
				report.AddLineAssertionError("FAIL<br>DE39: " + res[0] + " Se esperaba: " + de39);
				System.out.println("##[warning] : FAIL : \r\nDE39: " + res[0] + " Se esperaba: " + de39 + "\r\n");
				result = false;
			}

			System.out.println("##### FIN DE EJECUCION OPI #####");
			report.AddLine("##### FIN DE EJECUCION OPI #####");

						
			//POSTCONDICIONES

			//Separador
			System.out.println("##################################################################################################################################################################################################################"
					+ "##################################################################################################################################################################################################################\r\n");


		} catch (Exception e) {
			report.AddLine("Error:<br>" + e);
			System.out.println("##[warning] : Error:\r\n" + e);
			result = false;
		}

		return result;
	}


	private boolean validacionGral(dbWorker oraResp, Reports report, String idAutorizacionAdquirente) throws SQLException {
		//Variables
		boolean result = true;
		String queryVerf;
		
		System.out.println("##### INICIO EJECUCION DE VALIDACIONES #####");
		report.AddLine("##### INICIO EJECUCION DE VALIDACIONES #####");

		//Primera Validación
		queryVerf = "select a.importe_origen, a.id_entidad from "+esquema+"autorizacion a\r\n"
				+ "inner join "+esquema+"autorizacion_adquirente_log aal on a.id_autorizacion_adquirente = aal.id_autorizacion_adquirente\r\n"
				+ "inner join "+esquema+"respuesta_mc_log rml on a.id_autorizacion_adquirente = rml.id_autorizacion_adquirente\r\n"
				+ "where a.id_autorizacion_adquirente = '" + idAutorizacionAdquirente + "' and a.id_marca = '2' and a.modo_ingreso = '01' and id_estado = '0'";

		Validacion(oraResp, report, queryVerf);

		//Verificacion de todos los resultados obtenidos
		if (!Status.equals("P")) {
			report.AddLineAssertionError("===== " + Status + " =====");;
			System.out.println("===== " + Status + " =====");
			result = false;
		}

		System.out.println("##### FIN DE EJECUCION DE VALIDACIONES #####");
		report.AddLine("##### FIN DE EJECUCION DE VALIDACIONES #####");

		return result;
	}
	
	private void Validacion(dbWorker oraResp, Reports report , String queryVerf) throws SQLException {
		
		String validaRes[][];
		
		validaRes = oraResp.executeQuery(queryVerf);
		System.out.println("la query devuelve"+validaRes);
		if (!validaRes.equals("1")) {
			report.AddLineAssertionError("FAIL<br>No se cumplieron todas las validaciones");
			System.out.println("##[warning] : FAIL:\r\nNo se cumplieron todas las validaciones\r\n");
			Status = "FAIL - Cantidad de resultados";
		} else {
			report.AddLine("Ejecucion Correcta:<br>Se cumplieron todas las validaciones");
			System.out.println("##[section] : Ejecucion Correcta:\r\nSe cumplieron todas las validaciones\r\n");
			Status = "P";
		}
	}

}