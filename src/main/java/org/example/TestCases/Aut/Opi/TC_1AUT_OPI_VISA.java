package org.example.TestCases.Aut.Opi;
import CentaJava.Core.Datasources;
import CentaJava.Core.DriverManager;
import CentaJava.Core.Reports;
import io.restassured.path.json.JsonPath;
import org.example.Repositories.Repo_Variables;
import org.example.opi.AUT_OPI_OPERACIONES;
import org.testng.AssertJUnit;
import org.testng.annotations.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TC_1AUT_OPI_VISA {


    static

    //Init
    DriverManager DM;
    static Datasources data;
    static Reports report;
    static Repo_Variables repoVar;
    static String path;
    static String configEntidad;
    static String entidad;
    static String datosEntradaSegunTc;

    @BeforeSuite
    static void initAll() throws IOException {
        DM = new DriverManager();
        data = new Datasources();
        report = new Reports();
        repoVar = new Repo_Variables();
        //path = "./Datasources/config_entidad.json";
        path = "C:\\Users\\dpaterno\\Downloads\\advanced-selenium-webdriver-master\\advanced-selenium-webdriver-master\\BEGINNING\\ProyectoPruebaGP\\src\\main\\java\\org\\example\\Datasources\\config_entidad.json";
        configEntidad = new String(Files.readAllBytes(Paths.get(path)));
        entidad= JsonPath.from(configEntidad).getString("ENTIDAD.id_entidad");

    }

    @BeforeClass
    void init() throws IOException {

    }

    @BeforeMethod

    @BeforeTest
    void beforeTest() {

    }

    @Test
    void TC_1_AUT_OPI_VISA() {
        //DEFINITIONS
        AUT_OPI_OPERACIONES AUT_OPI_OPERACIONES = new AUT_OPI_OPERACIONES();

        //SET INDIVIDUAL DATASOURCE

        //Nombre Real del TC
        boolean status = false;
        String nroTC = "1";
        String name = "TC_" + nroTC + "_AUT_OPI_VISA_" + entidad + " - Credito - Compra Local - Manual";
        String TCFilesPath = "./TC_Files/OPI/" + entidad + "/VISA/TC_" + nroTC;
        String msg = "True;Resultado de la ejecucion OK. TC: " + name;

        //Inicializacion de las Variables del Repositorio
        repoVar.setTipoTc("SSH");
        repoVar.setResult(status);
        repoVar.setDataMsg(name);

        //SET THE EXECUTION PLAN
        status = AUT_OPI_OPERACIONES.COMPRA(report, name, configEntidad, entidad, TCFilesPath);

        //Configuracion de variables para el armado del reporte
        repoVar.setResult(status);

        //Verificamos si la ejecucion falla y guardamos el status Gral.
        if (status == false)
        {
            msg = "Fail;Fallo la ejecucion. TC: " + name;
        }

        //Se avisa x jUnit el resultado de la prueba
       // AssertJUnit.assertEquals("Resultado: " + msg.split(";")[1],"True", msg.split(";")[0]);
    }

    @AfterTest
    void afterTest() {

    }

    @AfterClass
    void tearDown() {
        if (repoVar.getTipoTc().equals("API")) {
            report.addTestCaseToGeneralReport(repoVar.getResult(), repoVar.getDataMsg(), "");
            report.saveTestCaseReport(repoVar.getDataMsg());
        } else {
            System.out.println("El caso de prueba no es: API");
        }
    }

    @AfterSuite
    static void tearDownAll() {
        System.out.println("Execution finished");
        report.saveGeneralReport();
    }
}
